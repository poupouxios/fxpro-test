# FxPro test

The test was run on Ubuntu 15.10. There is a bash script that will open two parallel terminals by using the gnome-terminal command.

### Requirements

The test requires some essential components to work:

* [MongoDB](https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-14-04)

## Setup

* Clone the repo into your local machine [https://poupouxios@bitbucket.org/poupouxios/fxpro-test.git](https://poupouxios@bitbucket.org/poupouxios/fxpro-test.git)
* Navigate inside the fxpro-test folder
* Assuming that the MongoDB instance is setup, run 
```shell
  ./setup.sh
```
* The bash script will install all the necessary npm and bower modules and will open two parallel terminals to execute the servers.
* If the above things worked, you can access the application at `http://localhost:4000`.

## Extra information

The application was developed using Angular.js, node.js and MongoDB as database to store the messages and sessions.

When the app will run on first time, it will prompt the user to setup a nickname for the chat. After that, the nickname will be stored on cookie and persist it in the database and if the user closes its browser or refreshes the page, they will still be logged in.

A config file exist where all the necessary variables are being setup to be ready for deployment.
