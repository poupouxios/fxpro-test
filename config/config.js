var config = {
  session : {
    secret : "Fxpr0t3stS3cr3tK3y@@!",
    cookie : {
      maxAge: 14 * 24 * 60 * 60,
      secure: false,
      name: "client-side-cookie"
    }
  },
  mongodb: {
    mongooseUrl : "mongodb://127.0.0.1:27017/fxpro-test"
  },
  backendCredentials:{
    serverUrl:"http://localhost",
    port: 2000
  }
};

module.exports = config;
