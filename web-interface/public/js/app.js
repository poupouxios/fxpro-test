'use strict';

var app = angular.module('fxpro-test-client', ['ngMaterial', 'ngAnimate', 'ngMdIcons', 'btford.socket-io'])
var serverBaseUrl = "http://localhost:2000";

app.factory('socket', function (socketFactory) {
    var myIoSocket = io.connect(serverBaseUrl);

    var socket = socketFactory({
        ioSocket: myIoSocket
    });

    return socket;
});

app.controller('MainController', function ($scope, $mdDialog, socket, $http) {
    $scope.messages = [];
    $scope.room = "MAIN";

    socket.on('user joined', function (data) {
      console.log(data.nickname + ' has joined');
    });

    $scope.nicknameModal = function (ev) {
        $http.get('/get-user-session').success(function (data) {
          if(data.user){
            $scope.nickname = data.user;
            $scope.alert = 'nickname: "' + data.user + '".';
            socket.emit('join room', {
                nickname: data.user,
                room: $scope.room
            });

            $http.get(serverBaseUrl + '/msg?room=' + $scope.room).success(function (msgs) {
                $scope.messages = msgs;
            });
          }else{
            $mdDialog.show({
                    controller: nicknameDialogController,
                    templateUrl: 'partials/nickname.tmpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                })
                .then(function (nickname) {
                    console.log(nickname);
                    $scope.nickname = nickname;
                    $scope.alert = 'Nickname: "' + nickname + '".';

                    $http.post("/save-nickname",{nickname: nickname});

                    socket.emit('join room', {
                        nickname: nickname,
                        room: $scope.room
                    });

                    $http.get(serverBaseUrl + '/msg?room=' + $scope.room).success(function (msgs) {
                        $scope.messages = msgs;
                    });

                }, function () {
                });
          }
        });
    };
    socket.on('message created', function (data) {
      $scope.messages.push(data);
    });
    
    $scope.send = function (msg) {
        socket.emit('new message', {
            room: $scope.room,
            message: msg,
            nickname: $scope.nickname
        });
    };
});
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });
                scope.message = "";
                event.preventDefault();
            }
        });
    };
});

function nicknameDialogController($scope, $mdDialog) {
    $scope.hide = function () {
        $mdDialog.hide();
    };
    $scope.cancel = function () {
        $mdDialog.cancel();
    };
    $scope.setNickname = function (nickname) {
        $mdDialog.hide(nickname);
    };
}
