module.exports = function(app){
  app.get('/',function(req, res){
    res.render('index');
  });
  
  app.get('/get-user-session',function(req,res){
    if(req.session.nickname != null){
      res.json({user : req.session.nickname});
    }else{
       res.json({user : null});
    }
  });
  
  app.post('/save-nickname', function(req,res){
    req.session.nickname = req.body.nickname;
    req.session.save();
  });
  
};
