var express = require('express'),
path = require('path'),
mongoose = require('mongoose'),
session = require('express-session'),
MongoStore = require('connect-mongo')(session)
bodyParser = require('body-parser'),
config  = require('../config/config');

mongoose.connect(config.mongodb.mongooseUrl);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

//Session setup
app.use(session({
  secret  : config.session.secret,
  resave: true,
  saveUninitialized: true,
  cookie:{
    maxAge: config.session.cookie.maxAge,
    secure: config.session.cookie.secure,
    name: config.session.cookie.name
  },
  store   : new MongoStore({
    mongooseConnection: mongoose.connection
  })
}));

app.use(express.static(path.join(__dirname, 'public')));

//Routes
require('./routes/route.js')(app);

app.set('port', process.env.PORT || 4000);
var server = app.listen(app.get('port'), function() {
});

console.log('Server started. Url : http://localhost:4000');

module.exports = app;
