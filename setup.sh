#!/bin/bash

cd ./server && npm install
cd ../web-interface && npm run setup

cd ../

gnome-terminal -x bash -c "cd ./server && npm start"  &
gnome-terminal -x bash -c "cd ./web-interface && node server.js" 

