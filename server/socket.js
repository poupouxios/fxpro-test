var UserMessageModel = require('./model/user-message');

module.exports = function(io){
  io.on('connection', function(socket) {

      socket.on('join room', function(data) {
        if(data.room){
          socket.join(data.room);
          io.in(data.room).emit('user joined', data);
        }
      });

      socket.on('new message', function(data) {
        var newMsg = new UserMessageModel({
          message: data.message,
          nickname: data.nickname,
          room: data.room,
          created: new Date()
        });

        newMsg.save(function(err, msg){
          io.in(msg.room).emit('message created', msg);
        });
      });
  });
};
