var UserMessageModel = require('./../model/user-message');

module.exports = function(app){
  app.get('/', function(req, res) {
    res.sendfile('index.html');
  });

  app.get('/msg', function(req, res) {
    UserMessageModel.find({
      'room': req.query.room
    }).exec(function(err, msgs) {
      res.json(msgs);
    });
  });

};
