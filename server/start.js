var express = require('express'),
mongoose = require('mongoose'),
app = express(),
server = require('http').Server(app),
io = require('socket.io')(server),
bodyParser = require('body-parser'),
config  = require('../config/config'),
 UserMessageModel = require('./model/user-message');

mongoose.connect(config.mongodb.mongooseUrl);
app.use(express.static(__dirname + '/public'));

//Enable CORS
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});

//ROUTES
require('./routes/route.js')(app);

//SOCKET
require('./socket.js')(io);

//Start Server
server.listen(config.backendCredentials.port);
console.log('Server started. Url : ' + config.backendCredentials.serverUrl + ":" + config.backendCredentials.port);
