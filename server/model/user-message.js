var mongoose = require('mongoose');

var UserMessageSchema = mongoose.Schema({
  created: Date,
  message: String,
  nickname: String,
  room: String
});

module.exports = mongoose.model('UserMessage', UserMessageSchema);

